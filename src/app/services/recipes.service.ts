import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  getSelectedRecipeId = new Subject<number>();

  constructor() { }
  getAllReipes(){
    let allRecipes = [
      {id:0,name:'Rava shira',ingredients:'rava,butter,sugar,water,salt',description:'there ara may ways to made shira'},
      {id:1,name:'Rava dosa',ingredients:'rava,butter,water,salt,mirchi,turmric,onion,coriender',description:'mix 1liter water and 1 cup rava and keep it for one night, after that remove water and add tiny pieces of onion tomato and mirchy mix in that battel, and sprect one spoon of battele on hot pan'},
      {id:2,name:'dahiwade',ingredients:'rava,butter,sugar,water,salt',description:'there ara may ways to made shira'},
    ];
    return allRecipes;
  }
  updateRecipe(recipeId:number){

  }
  deleteRecipe(recipeId:number){
    
  }
}
