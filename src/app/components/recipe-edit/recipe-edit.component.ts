import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  constructor(private router:Router, private activatedRoute:ActivatedRoute) { }
  editableId:number;
  isEditable:boolean;
  ngOnInit() {
    this.editableId = this.activatedRoute.snapshot.params['id'];
    this.activatedRoute.params.subscribe((params:Params)=>{
      this.editableId = +params['id'];
      this.isEditable = params['id'] !=null
    });
    console.log("editableid - "+this.isEditable);
  }

}
