import { Component, OnInit, OnDestroy} from '@angular/core';
import { RecipesService } from '../../services/recipes.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  //@Output() recipeSelectedNotifyParent: EventEmitter<{recipeId:number,selected:boolean}> = new EventEmitter();
  //@Output() sendRecipeData : EventEmitter<{id:number,name:string,ingredient:string,description:string}> = new EventEmitter();
  //recipeSelectedNotifyParent:boolean;false;
  recipes:any;
  recipeIdThroughService:number;
  private storedInVar;
  constructor(private recipesService:RecipesService) { 
  }
  ngOnInit() {
    this.recipes = this.recipesService.getAllReipes() ;
    this.storedInVar = this.recipesService.getSelectedRecipeId.subscribe((id)=>{
      this.recipeIdThroughService = id;
      console.log("this.recipeIdThroughService = "+ this.recipeIdThroughService);
    });
  }
  // getRecipeDetail(id: number){
  //   //this.isRecipeSelected = true;
  //   this.recipeSelectedNotifyParent.emit({recipeId:id,selected:true});
  //   //this.router.navigate(['/recipe',id]);
  // }
  // getRecipeDataId(id){
  //   this.sendRecipeData.emit(this.recipes[id]);
  // }
  ngOnDestroy(){
    this.storedInVar.unsubscribe();
  }
}
