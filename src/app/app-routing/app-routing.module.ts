import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from '../components/shopping-list/shopping-list.component';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { RecipeListComponent } from '../components/recipe-list/recipe-list.component';
import { RecipeBookComponent } from '../components/recipe-book/recipe-book.component';
import { RecipesComponent } from '../components/recipes/recipes.component';
import { RecipeStartComponent } from '../components/recipe-start/recipe-start.component';
import { RecipeDetailComponent } from '../components/recipe-detail/recipe-detail.component';
import { RecipeEditComponent } from '../components/recipe-edit/recipe-edit.component';
const routes : Routes = [
  {path: '', redirectTo:'/shopping-list', pathMatch:'full'},
  {path: 'recipe-book', component: RecipeBookComponent},
  {path: 'shopping-list', component: ShoppingListComponent},
  {path: 'recipe-list', component: RecipesComponent},
  {path: 'recipe', component:RecipesComponent , children:[
    {path:'', component:RecipeStartComponent},
    {path:'new',component:RecipeEditComponent},
    {path:':id', component:RecipeDetailComponent},
    {path:':id/edit', component:RecipeEditComponent}
  ]},
  {path: '**', component:PageNotFoundComponent}
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})

export class AppRoutingModule { }
