import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RecipesService } from '../../services/recipes.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
//@Input() sendRecipeDetails : {id:number,name:string,ingradient:string,description:string};
  constructor(private renderer : Renderer2, private router:Router, private activatedRoute:ActivatedRoute,
    private recipeService:RecipesService) { }
  recipeId:number;
  recipes:any;
  recipeItem:{};
  ngOnInit() {
    this.recipes = this.recipeService.getAllReipes();
     this.recipeId = +this.activatedRoute.snapshot.params['id'];
     this.activatedRoute.params
     .subscribe((params:Params)=>{
      this.recipeItem = this.recipes[+params['id']];
     })
  }
  deleteRecipe(id:number){
    
  }
}
