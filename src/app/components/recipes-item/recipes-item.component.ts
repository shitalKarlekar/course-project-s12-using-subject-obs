import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RecipesService } from 'src/app/services/recipes.service';


@Component({
  selector: 'app-recipes-item',
  templateUrl: './recipes-item.component.html',
  styleUrls: ['./recipes-item.component.css']
})
export class RecipesItemComponent implements OnInit {
  @Input() recipeItem : {id:number,name:string,ingradient:string,description:string};
  //recipeItem : {id:1,name:"string",ingradient:"string",description:"string"};
  //@Output() sendRecipeDataId : EventEmitter<number> = new EventEmitter();
  item:any;
  constructor(private router:Router, private activatedRoute:ActivatedRoute,
    private recipesService : RecipesService ) { }
  ngOnInit() {
    console.log(this.recipeItem);
    this.item = this.recipeItem;
  }
  getRecipeDetail(recipeId:number){
    //this.sendRecipeDataId.emit(recipeId);
  }
  whichRecipeSelects(id:number){
    this.recipesService.getSelectedRecipeId.next(id);
  }
}
